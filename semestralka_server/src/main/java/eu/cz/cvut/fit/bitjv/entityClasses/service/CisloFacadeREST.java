/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cz.cvut.fit.bitjv.entityClasses.service;

import eu.cz.cvut.fit.bitjv.entityClasses.Cisla;
import eu.cz.cvut.fit.bitjv.entityClasses.Cislo;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author samuelfabo
 */
@Stateless
@Path("eu.cz.cvut.fit.bitjv.entityclasses.cislo")
public class CisloFacadeREST extends AbstractFacade<Cislo> {

    @PersistenceContext(unitName = "com.mycompany_semestralka_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public CisloFacadeREST() {
        super(Cislo.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Cislo entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Long id, Cislo entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Cislo find(@PathParam("id") Long id) {
        return super.find(id);
    }

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Cisla findAllCisla() {
        Cisla cs = new Cisla();
        Collection<Cislo> c = super.findAll();
        Set<Cislo> s = new HashSet<>(c);
        cs.setCisla(s);
        return cs;
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Cisla findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        Cisla cs = new Cisla();
        Collection<Cislo> c = super.findRange(new int[]{from, to});
        Set<Cislo> s = new HashSet<>(c);
        cs.setCisla(s);
        return cs;
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
