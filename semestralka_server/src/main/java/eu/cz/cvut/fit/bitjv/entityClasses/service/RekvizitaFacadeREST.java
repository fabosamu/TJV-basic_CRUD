/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cz.cvut.fit.bitjv.entityClasses.service;

import eu.cz.cvut.fit.bitjv.entityClasses.Rekvizita;
import eu.cz.cvut.fit.bitjv.entityClasses.Rekvizity;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author samuelfabo
 */
@Stateless
@Path("eu.cz.cvut.fit.bitjv.entityclasses.rekvizita")
public class RekvizitaFacadeREST extends AbstractFacade<Rekvizita> {

    @PersistenceContext(unitName = "com.mycompany_semestralka_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public RekvizitaFacadeREST() {
        super(Rekvizita.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Rekvizita entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Long id, Rekvizita entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Rekvizita find(@PathParam("id") Long id) {
        return super.find(id);
    }

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Rekvizity findAllRekvizity() {
        Rekvizity ps = new Rekvizity();
        Collection<Rekvizita> c = super.findAll();
        Set<Rekvizita> s = new HashSet<>(c);
        ps.setRekvizity(s);
        return ps;
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Rekvizity findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        Rekvizity ps = new Rekvizity();
        Collection<Rekvizita> c = super.findRange(new int[]{from, to});
        Set<Rekvizita> s = new HashSet<>(c);
        ps.setRekvizity(s);
        return ps;
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
