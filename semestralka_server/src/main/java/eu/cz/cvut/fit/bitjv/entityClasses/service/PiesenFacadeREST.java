/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cz.cvut.fit.bitjv.entityClasses.service;

import eu.cz.cvut.fit.bitjv.entityClasses.Piesen;
import eu.cz.cvut.fit.bitjv.entityClasses.Piesne;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author samuelfabo
 */
@Stateless
@Path("eu.cz.cvut.fit.bitjv.entityclasses.piesen")
public class PiesenFacadeREST extends AbstractFacade<Piesen> {

    @PersistenceContext(unitName = "com.mycompany_semestralka_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public PiesenFacadeREST() {
        super(Piesen.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Piesen entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Long id, Piesen entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Piesen find(@PathParam("id") Long id) {
        return super.find(id);
    }

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Piesne findAllPiesne() {
        Piesne ps = new Piesne();
        Collection<Piesen> c = super.findAll();
        Set<Piesen> s = new HashSet<>(c);
        ps.setPiesne(s);
        return ps;
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Piesne findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        Piesne ps = new Piesne();
        Collection<Piesen> c = super.findRange(new int[]{from, to});
        Set<Piesen> s = new HashSet<>(c);
        ps.setPiesne(s);
        return ps;
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
