package eu.cz.cvut.fit.bitjv.mavenproject1;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.RadioButtonGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.renderers.DateRenderer;
import eu.cz.cvut.fit.bitjv.mavenproject1.client.ClientCislo;
import eu.cz.cvut.fit.bitjv.mavenproject1.client.ClientPiesen;
import eu.cz.cvut.fit.bitjv.mavenproject1.client.ClientRekvizita;
import eu.cz.cvut.fit.bitjv.mavenproject1.dto.Cisla;
import eu.cz.cvut.fit.bitjv.mavenproject1.dto.Cislo;
import eu.cz.cvut.fit.bitjv.mavenproject1.dto.Piesen;
import eu.cz.cvut.fit.bitjv.mavenproject1.dto.Piesne;
import eu.cz.cvut.fit.bitjv.mavenproject1.dto.Rekvizita;
import eu.cz.cvut.fit.bitjv.mavenproject1.dto.Rekvizity;
import java.util.Collection;
import java.util.Date;
import java.util.Set;
import org.vaadin.crudui.crud.CrudListener;
import org.vaadin.crudui.crud.CrudOperation;
import org.vaadin.crudui.crud.impl.GridCrud;
import org.vaadin.crudui.form.impl.form.factory.GridLayoutCrudFormFactory;



/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of an HTML page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */


@Theme("mytheme")
public class MyUI extends UI {

    private Grid<Cislo> grid = new Grid<>();
    private ClientCislo clientC = new ClientCislo();
    private ClientRekvizita clientR = new ClientRekvizita();
    private ClientPiesen clientP = new ClientPiesen();
    
    private void refreshGrid () {
        grid.removeAllColumns();
        Set<Cislo> cisla = clientC.findAllCisla_JSON(Cisla.class).getCisla();
        grid.setItems(cisla);
        grid.addColumn(Cislo::getName).setCaption("nazov");
        grid.addColumn(Cislo::getDuration).setCaption("trvanie");
        grid.addColumn(Cislo::getId).setCaption("id");
    }
    
    
    @Override
    protected void init(VaadinRequest vaadinRequest) {
        
        VerticalLayout layout = new VerticalLayout();
        
        GridCrud<Rekvizita> crudRekvizita = new GridCrud<>(Rekvizita.class);
        crudRekvizita.getGrid().setColumnOrder("id","name");
        crudRekvizita.setCaption("Rekvizity");
        crudRekvizita.getGrid().setResponsive(true);
        crudRekvizita.setCrudListener(new CrudListener<Rekvizita>() {
            @Override
            public Collection<Rekvizita> findAll() {
                return clientR.findAllRekvizity_JSON(Rekvizity.class).getRekvizity();
            }

            @Override
            public Rekvizita add(Rekvizita domainObjectToAdd) {
                clientR.create_JSON(domainObjectToAdd);
                return domainObjectToAdd;            
            }

            @Override
            public Rekvizita update(Rekvizita domainObjectToUpdate) {
                clientR.edit_JSON(domainObjectToUpdate, String.valueOf(domainObjectToUpdate.getId()) );
                return domainObjectToUpdate;
            }

            @Override
            public void delete(Rekvizita domainObjectToDelete) {
                clientR.remove(String.valueOf(domainObjectToDelete.getId()) );
            }
        });
        
        GridLayoutCrudFormFactory<Rekvizita> formFactoryR = new GridLayoutCrudFormFactory<>(Rekvizita.class, 2, 2);
        formFactoryR.setUseBeanValidation(true);
        crudRekvizita.setCrudFormFactory(formFactoryR);
        
        formFactoryR.setDisabledProperties(CrudOperation.ADD, "id");
        formFactoryR.setDisabledProperties(CrudOperation.UPDATE, "id");
        
        
        GridCrud<Piesen> crudPiesen = new GridCrud<>(Piesen.class);
        crudPiesen.setCaption("Piesne");
        crudPiesen.getGrid().setColumnOrder("id","name","date");
        crudPiesen.getGrid().setResponsive(true);
        crudPiesen.setCrudListener(new CrudListener<Piesen>() {
            @Override
            public Collection<Piesen> findAll() {
                return clientP.findAllPiesne_JSON(Piesne.class).getPiesne();
            }

            @Override
            public Piesen add(Piesen domainObjectToAdd) {
                clientP.create_JSON(domainObjectToAdd);
                return domainObjectToAdd;
            }

            @Override
            public Piesen update(Piesen domainObjectToUpdate) {
                clientP.edit_JSON(domainObjectToUpdate, String.valueOf(domainObjectToUpdate.getId()));
                return domainObjectToUpdate;
            }

            @Override
            public void delete(Piesen domainObjectToDelete) {
                clientP.remove(String.valueOf(domainObjectToDelete.getId()));
            }
        });
        ((Grid.Column<Piesen, Date>) crudPiesen.getGrid().getColumn("date")).setRenderer(new DateRenderer("%1$te.%1$tm.%1$tY"));
        
        GridLayoutCrudFormFactory<Piesen> formFactoryP = new GridLayoutCrudFormFactory<>(Piesen.class, 2, 2);
        formFactoryP.setUseBeanValidation(true);
        crudPiesen.setCrudFormFactory(formFactoryP);
        
        formFactoryP.setDisabledProperties(CrudOperation.ADD, "id");
        formFactoryP.setDisabledProperties(CrudOperation.UPDATE, "id");
        
        formFactoryP.setFieldCreationListener("date", field -> ((DateField) field).setDateFormat("dd.MM.yyy"));
       
        
        
//        
//        GridCrud<Cislo> crudC = new GridCrud<>(Cislo.class);
//        
//        crudC.setCrudListener(new CrudListener<Cislo>() {
//            @Override
//            public Collection<Cislo> findAll() {
//                return clientC.findAllCisla_JSON(Cisla.class).getCisla();
//            }
//
//            @Override
//            public Cislo add(Cislo domainObjectToAdd) {
//                clientC.findAllCisla_JSON(Cisla.class).getCisla().add(domainObjectToAdd);
//                return domainObjectToAdd;
//            }
//
//            @Override
//            public Cislo update(Cislo domainObjectToUpdate) {
//                clientC.edit_JSON(domainObjectToUpdate, String.valueOf(domainObjectToUpdate.getId()) );
//                return domainObjectToUpdate;
//            }
//
//            @Override
//            public void delete(Cislo domainObjectToDelete) {
//                clientC.remove(String.valueOf(domainObjectToDelete.getId()));
//            }
//        });
//        
//        crudC.getGrid().getColumn("rekvizity").setRenderer(group -> group == null ? "" : ((Rekvizita) group).getName(), new TextRenderer());
//        crudC.getGrid().getColumn("piesne").setRenderer(group -> group == null ? "" : ((Piesen) group).getNazov(), new TextRenderer());
//        
//        GridLayoutCrudFormFactory<Cislo> formFactoryC = new GridLayoutCrudFormFactory<>(Cislo.class, 4, 4);
//        crudC.setCrudFormFactory(formFactoryC);
//        
//        formFactoryC.setDisabledProperties(CrudOperation.ADD, "id");
//        formFactoryC.setDisabledProperties(CrudOperation.UPDATE, "id");
//        
//        formFactoryC.setFieldProvider("rekvizity",
//        new CheckBoxGroupProvider<>("rekvizity", clientR.findAllRekvizity_JSON(Rekvizity.class).getRekvizity(), Rekvizita::getName));
//        
//        formFactoryC.setFieldProvider("piesne",
//        new CheckBoxGroupProvider<>("piesne", clientP.findAllPiesne_JSON(Piesne.class).getPiesne(), Piesen::getNazov));
//        
//        crudC.setErrorHandler((event) -> {
//            Notification.show("error", Notification.Type.ERROR_MESSAGE);
//        });
//        
//        layout.addComponent(crudC);

      
        
        
        Button newItem = new Button("",(event) -> {
            final Window window = new Window("Add");
            window.setWidth(300.0f, Unit.PIXELS);
            final FormLayout content = new FormLayout();
            content.setMargin(true);
            window.setContent(content);
            window.center();
            TextField nazov = new TextField("Nazov:");
            TextField trvanie = new TextField("Trvanie:");
            content.addComponent(nazov);
            content.addComponent(trvanie);
            content.addComponent(new Button("Add", (ev) -> {
                if (nazov.getValue().isEmpty()){
                    Notification.show("Fill name!");
                    return;
                }
                Cislo cislo = new Cislo();
                cislo.setName(nazov.getValue());
                try {
                    cislo.setDuration(Long.valueOf(trvanie.getValue()));
                } catch (Exception e){
                    Notification.show("Zadaj validne cislo!", Notification.Type.ERROR_MESSAGE);
                    return;
                }
                clientC.create_JSON(cislo);
                window.close();
                refreshGrid();
            }));
            content.addComponent(new Button("Cancel", (ec) -> {
                window.close();
            }));
            
            addWindow(window);
        });
        newItem.setIcon(VaadinIcons.PLUS);
        

        Button deleteItem = new Button("", (event) -> {
            Set<Cislo> items = grid.getSelectedItems();
            if (items.isEmpty()){
                Notification.show("Nemam itemy");
            }
            Cislo cislo = items.iterator().next();
            Notification.show(String.valueOf(cislo.getId()),Notification.Type.TRAY_NOTIFICATION);
            if ( !(cislo.getPiesne().isEmpty() || cislo.getRekvizity().isEmpty()) ){
                Notification.show("Treba odstranit rekvizity/piesne z cisla!", Notification.Type.ERROR_MESSAGE);
                return;
            }
            clientC.remove(String.valueOf(cislo.getId()));
            grid.removeAllColumns();
            refreshGrid();
        });
        deleteItem.setIcon(VaadinIcons.TRASH);
        
        deleteItem.setEnabled(false);
        
        Button editItem = new Button("", (event) -> {
            final Window window = new Window("Edit");
            window.setWidth(300.0f, Unit.PIXELS);
            final FormLayout content = new FormLayout();
            content.setMargin(true);
            window.setContent(content);
            window.center();
            
            Set<Cislo> items = grid.getSelectedItems();
            if (items.isEmpty()){
                Notification.show("Nemam itemy!");
            }
            Cislo cislo = items.iterator().next();
            
            TextField nazov = new TextField("Nazov:");
            nazov.setValue(cislo.getName());
            TextField trvanie = new TextField("Trvanie:");
            trvanie.setValue(String.valueOf(cislo.getDuration()));
            Label id = new Label(String.valueOf(cislo.getId()));
            
            content.addComponent(nazov);
            content.addComponent(trvanie);
            content.addComponent(id);
            content.addComponent(new Button("Edit", (ev) -> {

            refreshGrid();

            if (nazov.getValue().isEmpty()){
                Notification.show("Fill name!");
                return;
            } else {
                cislo.setName(nazov.getValue());
            }

            try {
                cislo.setDuration(Long.valueOf(trvanie.getValue()));
            } catch (Exception e){
                Notification.show(e.getMessage(), Notification.Type.ERROR_MESSAGE);
                return;
            }
            clientC.edit_JSON(cislo, String.valueOf(cislo.getId()));
            window.close();
            refreshGrid();
            }));
            content.addComponent(new Button("Cancel", (ec) -> {
                window.close();
            }));
            addWindow(window);
        });
        editItem.setIcon(VaadinIcons.PENCIL);
        editItem.setEnabled(false);
        
        grid.setSizeFull();
//        grid.setColumnOrder("id","nazov","trvanie");
        grid.addSelectionListener((event) -> {
            if (event.getAllSelectedItems().isEmpty()) {
                deleteItem.setEnabled(false);
                editItem.setEnabled(false);
            } else {
                deleteItem.setEnabled(true);
                editItem.setEnabled(true);
            }
        });
        
        refreshGrid();
        
        Button refreshItems = new Button("", (event) -> {
            refreshGrid();
        });
        refreshItems.setIcon(VaadinIcons.REFRESH);
       
        
        RadioButtonGroup<Cislo> cislaGroup = new RadioButtonGroup<>("Cisla", clientC.findAllCisla_JSON(Cisla.class).getCisla());
        cislaGroup.setItemCaptionGenerator((item) -> {
            return item.getName();
        });
        cislaGroup.setResponsive(true);
        
        ListSelect<Rekvizita> rekvizityGroup = new ListSelect<>("", clientR.findAllRekvizity_JSON(Rekvizity.class).getRekvizity());
        rekvizityGroup.setItemCaptionGenerator((item) -> {
            return item.getName();
        });
        rekvizityGroup.setRows(clientC.findAllCisla_JSON(Cisla.class).getCisla().size());
        rekvizityGroup.setSizeFull();
        
        Button rekvizityButton = new Button("Pridaj Rekvizity", (event) -> {
            if (cislaGroup.isEmpty()){
                Notification.show("Zvol cislo pre pridanie!", Notification.Type.HUMANIZED_MESSAGE);
                return;
            }
            Cislo cislo = cislaGroup.getValue();
            cislo.setRekvizity(rekvizityGroup.getSelectedItems());
            clientC.edit_JSON(cislo, String.valueOf(cislo.getId()));
            if (rekvizityGroup.getValue().isEmpty()) {
                Notification.show("Prazdny zoznam pridany!", Notification.Type.HUMANIZED_MESSAGE);
            } else {
                Notification.show("Pridane!", Notification.Type.HUMANIZED_MESSAGE);
            }
        });
        rekvizityButton.setEnabled(false);
        
        
        
        
        ListSelect<Piesen> piesneGroup = new ListSelect<>("", clientP.findAllPiesne_JSON(Piesne.class).getPiesne());
        piesneGroup.setItemCaptionGenerator((item) -> {
            return item.getName();
        });
        piesneGroup.setRows(clientC.findAllCisla_JSON(Cisla.class).getCisla().size());
        piesneGroup.setSizeFull();
        
        Button piesneButton = new Button("Pridaj Piesne", (event) -> {
            if (cislaGroup.isEmpty()){
                Notification.show("Zvol cislo pre pridanie!", Notification.Type.ERROR_MESSAGE);
                return;
            }
            Cislo cislo = cislaGroup.getValue();
            cislo.setPiesne(piesneGroup.getSelectedItems());
            clientC.edit_JSON(cislo, String.valueOf(cislo.getId()));
            if (piesneGroup.getValue().isEmpty()) {
                Notification.show("Prazdny zoznam pridany!", Notification.Type.HUMANIZED_MESSAGE);
            } else {
                Notification.show("Pridane!", Notification.Type.HUMANIZED_MESSAGE);
            }
        });
        piesneButton.setEnabled(false);
        
        
        cislaGroup.addValueChangeListener((event) -> {
            piesneButton.setEnabled(true);
            rekvizityButton.setEnabled(true);
            Cislo cislo = event.getValue();
            rekvizityGroup.deselectAll();
            piesneGroup.deselectAll();
            
            for (Rekvizita rekvizita : cislo.getRekvizity()) {
                rekvizityGroup.select(rekvizita);
            }
            for (Piesen piesen : cislo.getPiesne()) {
                piesneGroup.select(piesen);
            }
        });
        
        Label captionCislo = new Label("<h3><p style=\"color:blue;\">Cisla</h3>", ContentMode.HTML);
        
        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.setSpacing(false);
        buttonsLayout.addComponents(refreshItems, newItem, editItem, deleteItem);
        
        VerticalLayout rekvizityLayout = new VerticalLayout(rekvizityButton, rekvizityGroup);
        rekvizityLayout.setSpacing(false);
        VerticalLayout piesneLayout = new VerticalLayout(piesneButton, piesneGroup);
        piesneLayout.setSpacing(false);
        
        
        HorizontalLayout relationLayout = new HorizontalLayout();
        relationLayout.addComponents(cislaGroup, rekvizityLayout, piesneLayout);
        relationLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        relationLayout.setResponsive(true);
        relationLayout.setSizeFull();
        
        layout.addComponent(crudRekvizita);
        layout.addComponent(crudPiesen);
        layout.addComponent(captionCislo);
        layout.addComponents(buttonsLayout, grid, relationLayout);
        setContent(layout);
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
