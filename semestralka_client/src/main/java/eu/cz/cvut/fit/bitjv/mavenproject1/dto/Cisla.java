/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cz.cvut.fit.bitjv.mavenproject1.dto;

import java.util.Set;
import java.util.List;


/**
 *
 * @author samuelfabo
 */
public class Cisla {
    private Set<Cislo> cisla;

    public Set<Cislo> getCisla() {
        return cisla;
    }

    public void setCisla(Set<Cislo> cisla) {
        this.cisla = cisla;
    }
}
