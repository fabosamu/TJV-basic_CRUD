/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cz.cvut.fit.bitjv.mavenproject1.dto;

import java.util.Set;
import java.util.List;

/**
 *
 * @author samuelfabo
 */
public class Rekvizity {
    
    private Set<Rekvizita> rekvizity;

    public Set<Rekvizita> getRekvizity() {
        return rekvizity;
    }

    public void setRekvizity(Set<Rekvizita> rekvizity) {
        this.rekvizity = rekvizity;
    }
    
   
}
