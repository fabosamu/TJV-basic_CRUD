/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cz.cvut.fit.bitjv.mavenproject1.dto;

import java.util.Set;
import java.util.List;

/**
 *
 * @author samuelfabo
 */
public class Piesne {
    
    private Set<Piesen> piesne;

    public Set<Piesen> getPiesne() {
        return piesne;
    }

    public void setPiesne(Set<Piesen> piesne) {
        this.piesne = piesne;
    }
    
    
}
