# Basic CRUD operations

This is a simple project working on simple database snippet with three entities, which i borrowed as an example from my previous project from BI-DBS -> Database Systems. Create, Read, Update and Delete can be used as queries to the DB.

I have implemented server and client part with help of Netbeans IDE. The communication runs through REST API. 

More specification is in README files in both client and server.
